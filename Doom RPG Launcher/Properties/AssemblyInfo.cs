﻿using System.Resources;
using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Versioning;

[assembly: AssemblyVersion("1.3.3.7")]
[assembly: Debuggable(DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints)]
[assembly: AssemblyCompany("DartPower Team LLC")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCopyright("DartPower Team LLC © 2018")]
[assembly: AssemblyDescription("Doom RPG Launcher")]
[assembly: AssemblyFileVersion("1.3.3.7")]
[assembly: AssemblyProduct("Doom RPG Launcher")]
[assembly: AssemblyTitle("Doom RPG Launcher")]
[assembly: AssemblyTrademark("Мы создаем будущее!")]
[assembly: CompilationRelaxations(8)]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows = true)]
[assembly: ComVisible(true)]
[assembly: Guid("e529dff8-1dee-419d-b3d9-98dcee1e73be")]
[assembly: NeutralResourcesLanguage("ru")]

